#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
'''
    '>>>' messages belong to the NeuroPy functionality.
    '[ ]' messages belong to the Socket Server being used. Convey several meanings:
        '[·]' provides Server DEBUG info, whocasing normal functionality.
        '[!]' provides Server ERROR info. Something went wrong.
        '[*]' provides Server SPECIAL info, such as information being rx from the Srv side
'''

import matplotlib.pyplot as plt
import numpy as np
import os
import socket
import time
from PIL import Image


HOST = '127.0.0.1'
PORT_SCKT = 6000

PATH_PARTIAL = "C:\\Users\\J.E. TFM\\Dropbox\\·UC3M\\· TFM\\FILES_TFM\\NEUROSKY\\Python_Mindwave_mobile\\"
IMGSET = "images\\"    # Path for Image set, each stored in a folder.
IMAGE_ARRAY = \
    [PATH_PARTIAL + IMGSET + "img1.jpg", PATH_PARTIAL + IMGSET + "img2.jpg",
     PATH_PARTIAL + IMGSET + "img3.jpg", PATH_PARTIAL + IMGSET + "img4.jpg",
     PATH_PARTIAL + IMGSET + "img5.jpg", PATH_PARTIAL + IMGSET + "img6.jpg",
     PATH_PARTIAL + IMGSET + "img7.jpg", PATH_PARTIAL + IMGSET + "img8.png"]


def show_images(arr, delay, sock, send):
    ''' Prints the images in arr[] to screen, one by one.
    Also retrieves such image's filename to send it over the socket (must be already
    active) to the Server so it can register its corresponding EEG values.
    'send' option is used as a switch to decide if the filenames want to be sent
    over the socket or the function wants to be used for img. display only. '''
    for i in arr:
        filename = get_filename(i)

        if not send == 1 and not send == 0:
                # Wrong option, neither 1 nor 0
            print "[!] ERROR. Incorrect 'send' option: <{}>. Skipping image display.".format(send)
            break

        img = Image.open(i)
        img = np.asarray(img)
                # Image plotting:
        plt.imshow(img)
        plt.draw()
        plt.pause(delay)

        if send == 1:
                # Send over filename
            write_socket(sock, filename)
            print "[·] Sent file: '{}' to Server.".format(filename)
        elif send == 0:
            pass
        else:
                # Should never reach this condition, filtered before.
            print "[!!!] FATAL. Incorrect 'send' option: <{}>. Skipping image display.".format(send)
            break

        plt.close()

        # TODO:
        # Setup protection so the socket cannot be used unless it is active already
        # Other option is a try/catch simply


def get_filename(img):
    ''' Retrieve filename from the complete file path: last element when split by
    the '\\' character. '''
    return img.split("\\")[-1]
    # return r"images\\\\(img[0-9]+\.[a-z]+)"
        # This could also be done by storing the filenames of each img in another array and
        # just traversing that one instead of looking back for the filename


def setup_socket():
    ''' Initialize active socket which communicates with
    the SERVER: NeuroPy Server which processes EEG data '''
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT_SCKT))
    print "[·] Connected to host {}:{}".format(HOST, PORT_SCKT)
    return s


def write_socket(socket, msg):
    ''' Write message to the active socket connected to
    the NEURO_AUTH_SERVER.
    Later, receive data from SERVER socket '''
    size = 1024

    print "[·] Writing to socket... (msg: '{}')".format(msg)
    socket.send(msg.encode('ascii'))
    # s.sendall('comeme los helloworld')

    rdata = socket.recv(size)
    print '[*] Data received: ', str(rdata)
                    # use repr() instead?
    return rdata


''''''''''''''''''''''''''''''''''''''''''''''''''''''


def main():

    print ">>> Starting up NEURO_AUTH_CLIENT..."

    msg_start = "START"
    msg_stop = "STOP"
    msg_err_fname = "comemeloshelloworld" # test wrong filename

    start_delay = 2
    test_delay = 2
    interimg_delay = 10

    # Start Client socket
    sckt = setup_socket()

    # TODO:
    # DEBUG: Print user - img_passwd relation instead (assume setup)

    # Show images
    print ">>> Presenting possible pass-images. \n One must be chosen!"
    show_images(IMAGE_ARRAY, 0.75, sckt, 0)

    # Open connection with SERVER socket
    print "\n>>> Launching connection with Server."
    write_socket(sckt, msg_start)
    time.sleep(start_delay)

    ### TESTING
            # Testing Phase I
    ''' print "\n>>> Begin TESTING phase (I):"
            # Test wrong filename
    write_socket(sckt, msg_err_fname)
    # write_socket(sckt, msg_fname)
    time.sleep(test_delay) '''
            # Testing Phase II
    ''' print "\n>>> TESTING phase (II):"
            # Test show_images() ERROR
    show_images(IMAGE_ARRAY, 2, sckt, 5)
    show_images(IMAGE_ARRAY, 2, sckt, "test")
    time.sleep(test_delay)  '''

    ### MAIN EXPERIMENT
            # Image slideshow (& filename sendover to server)
    print "\n>>> Begin EXPERIMENT phase:"
    show_images(IMAGE_ARRAY, interimg_delay, sckt, 1)
    print ">>> End EXPERIMENT phase.\n"

            # Close connection
    write_socket(sckt, msg_stop)
    print "[·] Closing connection with Server."
            # Close Client socket
    sckt.close()
    print "[·] Closing client socket."

    print "EOF.\n"



if __name__ == '__main__':
    main()
