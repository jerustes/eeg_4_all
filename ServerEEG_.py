#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-
'''
    '>>>' messages belong to the NeuroPy functionality.
    '[ ]' messages belong to the Socket Server being used. Convey several meanings:
        '[·]' provides Server DEBUG info, whocasing normal functionality.
        '[!]' provides Server ERROR info. Something went wrong.
        '[*]' provides Server SPECIAL info, such as information being rx from the Srv side
'''

import csv
import hashlib
import numpy as np
import socket
import sys
import time
import threading
import uuid
from datetime import datetime
from NeuroPy import NeuroPy
from time import sleep

HEADSET_NAME = "Neurosky Mindwave Mobile 2"
BT_PORT_NAME = "COM5"
REFRESH_RATE = 0.2  # Data retireval measured in seconds.

HOST = '127.0.0.1'  # Standard loopback interface addr (localhost).
PORT_SCKT = 6000    # Port to listen on.

USER_NAME = "testeeg"                # Default value

PROJ_PATH = "C:\\Users\\J.E. TFM\\Dropbox\\·UC3M\\· TFM\\FILES_TFM\\NEUROSKY\\\
Python_Mindwave_mobile\\"
EEGDUMP_FILE_DEF = "EEG_datafile.txt"   #File with EEG data dump, default when no username used (testing)
EEGDUMP_FILE_CUST = None
DB_FOLDR = "db_info\\"          # Folder 'database' info, user-img_pass relationship.
USR_PASS_FILE = "users.csv"     # ImgPass' hash of each registered user.
IMG_INFO_FILE = "images.csv"    # Image's ID and associated rnd string.

ATTENTION = []      # Buffer containing EEG Attention values.
MEDITATION = []     # "                     Meditation
AVG_ATTN_DCT = {}   # Dictionary containing imgID# - Attn. average tuples.

# TODO: testing. <<  BYPASS HEADSET  >>
#
# test_user = "test"
test_ATTN_DICT = {
    'img1' : '',
    'img2' : '',
    'img3' : '',
    'img4' : '',
    'img5' : '',
    'img6' : '',
    'img7' : '',
    'img8' : '',
    'img9' : '',
    'img10' : ''}
#AVG_ATTN_DCT = test_ATTN_DICT


''''''''''''''''''''''''''''''


def to_file(s, filepath=PROJ_PATH + EEGDUMP_FILE_DEF): 
    ''' Writes to file handling openign and closing it b4 & after '''
    file = open(filepath, "a+")
    file.write(s)
    file.close()


def file_setup(): 
    ''' Used at start to setup the file onto which registered EEG data is dumped '''
    global USER_NAME
    global PROJ_PATH
    global EEGDUMP_FILE_CUST
    global EEGDUMP_FILE_DEF

    if USER_NAME != "testeeg":
        EEGDUMP_FILE_CUST = USER_NAME + "_EEGdatafile.txt"
        to_file("EEG DUMP -- USER: {}\n".format(USER_NAME), filepath=PROJ_PATH + EEGDUMP_FILE_CUST)
        to_file("Measurement taken at : {} [{}] \n".format(
            datetime.now(), time.time()), filepath=PROJ_PATH + EEGDUMP_FILE_CUST)
    else:
        to_file("Measurement taken at : {} [{}] \n".format(datetime.now(), time.time()))
            # dumping to default filename


def setup_patientname():
    ''' Asks user for name of the patient who's about to use the program and uses it
    to name the EEG data file containing the readings '''
    global USER_NAME

    while True:
        name = raw_input("Please enter your User name: ")

        if name == "":
            print "Default user, then."
            break
        elif " " in name or ";" in name or "," in name:
            print "Characters like ' ', ',' and ';' cannot be used, try again."
        else:
            print "Patient name: {}".format(name)
            USER_NAME = name
            break


'''
    >>> SERVER MANAGEMENT

'''
def socket_server():
    ''' Initializes the socket usage. Becomes the SERVER for the
    communication. CLIENT is the experiment's interface '''
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # AF_INET: addr family for IPv4. SOCK_STREAM: TCP
            # sock: passive socket keeping the server alive until session closed
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    try:
        sock.bind((HOST, PORT_SCKT))
    except socket.error as msg:
        print "[!] Bind failed. ErrCode: {} Message {}.".format(str(msg[0]), str(msg[1]))
        sys.exit()
    print "[·] Socket bind complete."

    sock.listen(5)
    print "[·] Socket listening."
            # Enable accept() # connections

    while True:
        try:
            conn_client, address = sock.accept()
            print "[·] Connection Accepted! Address: {}".format(address)
                    # Incoming connection
                    # Generates new socket object (active sck)
            threading.Thread(target = listen_active, args = (conn_client,address)).start()
                    # Starts up a thread which invokes 'listen_client()'

        except Exception as e:
            print "[!] Exception {}.".format(str(e))
            print "[!] Socket error. Closing connection."


def listen_active(conn_client, address):
    ''' Handles the Client's incoming connection as an active socket. 
    Contains logic for connection states [ DISCONN ->('START')-> RECEIVING ->
    ->('STOP') -> FINISHED]
    · conn_client: active socket communicating with the client. '''
    global ATTENTION
    size = 1024
    receiving = False
    lexit = False
    avg = 0

    try:
        while not lexit:
                # Loop over 'blocking calls' to conn.recv().
                # Receive 1024 bit blocks.
            data_conn = conn_client.recv(size)

            if receiving == False:
                conn_client.sendall(data_conn)
                        # Reads sent data & echo it back to Client
                if data_conn == 'START':
                        # Receiving process starts.
                    receiving = True
                    print "[·] 'START' received!"
                    print "[·] Entering RECEIVING state. [Rst. Attn buffer] RECEIVING IMAGES...\n"
                    ATTENTION = []
                            # Clean up buffer, everything coming after this
                            #   is the first image
                            # In the first run, where the images are just being shown
                            #   to the user, data is just being thrown to the trash.

                            # TODO: what about the in-the-middle time? And the testrun?
            else:
                    # receiving = True
                conn_client.sendall(data_conn)
                        # Reads sent data & echo it back to Client
                if data_conn == 'STOP':
                        # Connection (experiment) ends
                    ATTENTION = []
                    receiving = False
                    print "\n[·] 'STOP' received!"
                    print "[·] Entering FINISHED state."
                    lexit = True
                elif not data_conn:
                    lexit = True
                    raise Exception('\n[!] Client disconnected. Exiting...')
                else:
                    data_conn = data_conn.split('.')[0]
                            # check filename fits in 'imgX' format, no extension (jpg, png...)
                    print "\n[*]\tFilename: '{}'".format(data_conn)

                    img_attention = ATTENTION
                            # This Attention value array becomes static, new changes
                            #   from further iterations go inside ATTENTION[]
                        # DEBUG:
                    print "[·] Current 'img_attention' dict:\t{}".format(img_attention)

                    ATTENTION = []
                            # All the saved ATTENTION[] data belongs to name whose
                            #   filename was just received.
                            # Reset ATTENTION[] for next image's data to be loaded
                    if len(img_attention) != 0:        
                        avg = mean_of(img_attention)

                            # DEBUG: testing / DEMO
                        avg = AVG_ATTN_DCT[data_conn] 
                
                # TODO: emit error msg when values = 0 (HEADSET DISCONNECTED. no signal)
                    AVG_ATTN_DCT[str(data_conn)] = avg
                            # Insert element in Dictionary{ "imgX" : attn_avg}
                    print "[·] Average Attention value saved ('{}')".format(avg)

                    print_dct_elem(AVG_ATTN_DCT, str(data_conn))
                            # Print the just-added dictionary entry
                    print "\t> receiving..."

    except Exception as e:
        print "\n[!] ERROR- Reception failed. Closing active server socket."
        print "[!] Exception found: '{}'".format(e.message)

    finally:
        print "\n[·] Ending connection. Closing active server socket."
        conn_client.close()

    # Authentication Module launch:
    if auth_process(USER_NAME):
        print "\n\n\t>>> AUTHENTICATION SUCCESSFUL!\n"
    else:
        print "\n\n\t>>> AUTHENTICATION FAILED!\n"

 
def mean_of(array):
    ''' Given Attn/Med array of values for an image, compute the
    mean for that feature. '''
    # sum(array)/float(len(array))
    return np.array(array).mean()


# UNUSED?
# TODO: is this printing a dictionary or a list? looks like LIST
def print_dict(dict, debug):
    ''' Given dictionary, prints it fully.
    If DEBUG option is active (!0), print 'DEBUG' option to screen. '''
    if debug != 0:
        print "\n[·]\tDEBUG. Print dictionary '{}':".format(debug)
        # debug: ÑAPA as a Service
        print "{"
    for key, val in dict:
        # TODO: \/ delete dis
        print "}}\t'for' iterations"
        try:
            print "\t{}: {}".format(key, val)
                    # print dict
        except Exception as e:
            print "[!] ERROR. Exception '{}' when printing \
                dictionary.".format(e.message)
    print "}"


def print_dct_elem(dict, key):
    ''' Given dictionary and a key, prints such key-value relationship. '''
    # DEBUG - print "[·]\tDEBUG. Printing dict element."
    try:
        print "[·] \tAttn. Dict > {}: {}".format(key, dict[key])
    except KeyError:
        print "[!] ERROR. Key '{}' was not found.".format(key)


'''
    >>> AUTHENTICATION

'''
def max_attn():
    ''' Finds out what image is the one where most attention was paid. 
        Uses global Avg. Attention Dictionary to get data from.'''
    global AVG_ATTN_DCT
    
    key_max = max(AVG_ATTN_DCT, key = AVG_ATTN_DCT.get)
    # key_max = max(AVG_ATTN_DCT.keys(), key = (lambda k: AVG_ATTN_DCT[k]))
    print ">>> Maximum attention:\t>Image '{}' - Attn. Value '{}'.".format(
        key_max, AVG_ATTN_DCT[key_max])
    return key_max


def hash_of(strng):
    ''' Hash of string passed as argument.'''
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + strng.encode()).hexdigest() + ':' + salt


def hash_salt(strng, salt):
    ''' Hash of string passed as argument GIVEN THE SALT.'''
    return hashlib.sha256(salt.encode() + strng.encode()).hexdigest()


def get_string_of(img_name):
    string = False

    with open(PROJ_PATH + DB_FOLDR + IMG_INFO_FILE) as usr_file:
        rdr = csv.DictReader(usr_file, delimiter=';')
        for row in rdr:
            if (row["path"].split("\\")[-1]) == img_name:
                    # The last element of the list made by the path is the filename itself
                    # (no extension).
                string = row["rnd_str"]
                break

    if not string:
        print "\n>>> ERROR. Img_str not found for filename '{}'".format(img_name)
    return string


def auth_process(username): #input_imgpwd, usr_hash):
    ''' Complete authentication check, given:
        username: of the person attempting the authentication.

        return False -> AUTH FAILURE
    '''
    attn_img = False        # This auth attempt's most attention-ed image name.
    usr_hash_salt = False   # 'Hash:salt' for a given username, taken from DB
    usr_hash = False        # (correct img_pass) hash of the user, from users.csv
    usr_salt = False
    login_hash = False

    print "\n\n>>> AUTHENTICATION\t[Processing...]"

    # usr_hash:
    with open(PROJ_PATH + DB_FOLDR + USR_PASS_FILE) as usr_file:
        rdr = csv.DictReader(usr_file, delimiter=';')
        for row in rdr:
            if str(row["username"]) == str(username):
                usr_hash_salt = row["pass_hash_salt"]
                #TODO: delete if ok >>      
                print "\tDEBUG > username found '{}'".format(usr_hash_salt)

        if not usr_hash_salt:        # Hash == False > username was not found in the dictionary
            print "\n>>> AUTHENTICATION DENIED: Username not registered."
            return False
        else:
            usr_hash, usr_salt = usr_hash_salt.split(":")
                    # In one read gets both the hash for the user and the salt which 
                    #   will be used for the 'login_hash' below
            # print "\tDEBUG > The USER HASH is '{}'".format(usr_hash)
            # print "\tDEBUG > The USER SALT is '{}'".format(usr_salt)
            # print ""
            #^ TODO: delete if ok

    # login_hash:
    attn_img = max_attn()   
    img_str = get_string_of(attn_img)
    if not img_str:
        return False
    if not usr_hash:
        return False
            # Salt value for attn_img
    login_hash = hash_salt(img_str, usr_salt)   # This auth's attempt's HASH VALUE
    # print "\tDEBUG > The LOGIN HASH is '{}'".format(login_hash)
    #^ TODO: delete if ok

    # FINAL CHECK
    return login_hash == usr_hash



'''
    >>> CALLBACKS 

'''
def attention_call(attention_val):
    ''' Called whenever NeuroPy receives a new value for 'Attention'.
    Loads it up in the respective buffer so it can be later matched with the image
    beign shown in that time span. '''
    global ATTENTION
    ATTENTION.append(attention_val)

    # print "--"
    # print "> Attention value: {}\n".format(attention_val)

    if USER_NAME != "testeeg":
        # to_file("-- {} \n".format(datetime.now().strftime("%H:%M:%S.%f")))
        to_file("-- {} \n".format(time.time()), filepath=PROJ_PATH + EEGDUMP_FILE_CUST)
                # Timestamp rather than formatted time
        to_file("Attention value: {} \n".format(attention_val), filepath=PROJ_PATH + EEGDUMP_FILE_CUST)
    else:
        to_file("-- {} \n".format(time.time()))
        to_file("Attention value: {} \n".format(attention_val))


def meditation_call(meditation_val):
    ''' Called whenever NeuroPy receives a new value for 'Meditation'.
    Loads it up in the respective buffer so it can be later matched with the image
    beign shown in that time span. '''
    MEDITATION.append(meditation_val)

    # print "> Meditation value: {}\n".format(meditation_val)
    # print "--"

    if USER_NAME != "testeeg":
        to_file("Meditation value: {} \n".format(meditation_val), filepath=PROJ_PATH + EEGDUMP_FILE_CUST)
        to_file("-- \n\n", filepath=PROJ_PATH + EEGDUMP_FILE_CUST)
    else:
        to_file("Meditation value: {} \n".format(meditation_val))
        to_file("-- \n\n")
    


# TODO: not working correctly, can only access values through
#   neurosky.rawValues object
def rawValue_call(rawValue_val):
    ''' Called whenever new value for 'RawValue' is received '''
    print "[Raw value: {}]".format(rawValue_val)


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


def main():
    
    print ">>> Starting up NEURO_AUTH_SERVER... "
    neuropy = NeuroPy(BT_PORT_NAME)
    print ">[\tWorking with Refresh Rate {}\t]<".format(REFRESH_RATE)
    print ">[\tConnecting to Bluetooth port '{}'\t]<".format(BT_PORT_NAME)
    
    setup_patientname()
    file_setup()
    print ">>>\tSetting up EEG data file\t<<<"
    
    print ">>>\tSetting up CallBacks\t<<<"
            # Call back associated with MindWave variable s.t. function is
            #   called when value is updated.
    neuropy.setCallBack("attention", attention_call)
    neuropy.setCallBack("meditation", meditation_call)
    # neuropy.setCallBack("RawValue", rawValue_call)
                # Callbacks work by waiting upon a value change, hence the need
                #   to wait for such change, defined by the Refresh rate
                #   established previously.

            # Start fetching data from MindWave
    neuropy.start()
    print ">>>\tStarting NeuroPy process\t<<<"

    try:
    
        print ">>>\tListening..."
        # It takes the program a few seconds to successfully
        #   retrieve MindWave values, displaying !0 values.
        # TODO: fix!

        print ">>> Launch SOCKET SERVER"
        socket_server()

    except KeyboardInterrupt:
        print ">>> STOP. Interruption received, shutting down..."
        try:
            neuropy.stop()
        except:
            pass

        sys.exit("\n>>>\tneuroBYE\t<<<\n")
    finally:
        # Stop data fetch
        neuropy.stop()
    
        # TODO: DEBUG DEBUG
        # Authentication Module launch:
    #if auth_process(test_user):
    #    print "\n\n\t>>> AUTHENTICATION SUCCESSFUL!\n"
    #else:
    #    print "\n\n\t>>> AUTHENTICATION FAILED!\n"


if __name__ == '__main__':
    main()



##############################################################

# OBJECT ACCESS
"""
# Access via object
neuropy = NeuroPy()
neuropy.start()

while True:
    if neuropy.meditation > 70: #Access data through object
        neuropy.stop()
    sleep(0.2) #Don't consume CPU cycles
"""
