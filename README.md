# Brainwave Authentication for the Masses

Brainwave Authentication framework using Neurosky MindWave 2 EEG headset.
Extensible authentication methodology built with extensibility and ease of use
in mind.